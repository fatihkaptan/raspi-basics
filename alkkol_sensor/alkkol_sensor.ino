/* MQ-3 Alcohol Sensor Circuit with Arduino */



const int AOUTpin=0;//the AOUT pin of the alcohol sensor goes into analog pin A0 of the arduino

const int DOUTpin=8;//the DOUT pin of the alcohol sensor goes into digital pin D8 of the arduino

const int ledPin=13;//the anode of the LED connects to digital pin D13 of the arduino


#include <LiquidCrystal.h> //ekran kütüphanesi


int rs=12, en=11, d4=5, d5=4, d6=3, d7=2;
LiquidCrystal lcd(rs,en,d4,d5,d6,d7); 


int limit;

int value;



void setup() {

Serial.begin(115200);//sets the baud rate

pinMode(DOUTpin, INPUT);//sets the pin as an input to the arduino

pinMode(ledPin, OUTPUT);//sets the pin as an output of the arduino

lcd.begin(16,2);

}



void loop()

{

value= analogRead(AOUTpin);//reads the analaog value from the alcohol sensor's AOUT pin

limit= digitalRead(DOUTpin);//reads the digital value from the alcohol sensor's DOUT pin

Serial.print("Alcohol value: ");

Serial.println(value);//prints the alcohol value

Serial.print("Limit: ");

Serial.print(limit);//prints the limit reached as either LOW or HIGH (above or underneath)

delay(100);

if (limit == HIGH){

digitalWrite(ledPin, HIGH);//if limit has been reached, LED turns on as status indicator

}

else{

digitalWrite(ledPin, LOW);//if threshold not reached, LED remains off

}

delay(250); //ekran yenileme
  lcd.clear(); //temizler ekranı
  lcd.setCursor(0,0); //imleç 1.satır 1.sütundan başlar
  lcd.print("Uctun mu:");
  lcd.setCursor(0,1); //alt satıra geç;
  lcd.print(value);
  

}





