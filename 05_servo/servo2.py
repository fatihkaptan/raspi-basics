import RPi.GPIO as GPIO                                  
import time                                

GPIO.setmode(GPIO.BCM)                   
GPIO.setup(12, GPIO.OUT)
GPIO.setwarnings(False)

pwm=GPIO.PWM(12,100)                        

pwm.start(5)

angle1=5
duty1= float(angle1)/10 + 2.5              

angle2=175
duty2= float(angle2)/10 + 2.5

ck=0
while ck<=7:
     pwm.ChangeDutyCycle(duty1)
     print(ck)
     time.sleep(0.8)
     pwm.ChangeDutyCycle(duty2)
     time.sleep(0.8)
     ck=ck+1
time.sleep(1)
GPIO.cleanup()