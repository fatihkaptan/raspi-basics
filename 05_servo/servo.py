from tkinter import * 

import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(12,GPIO.OUT) #define BCM18, ord BOARD12 as a output

pwm = GPIO.PWM(12,50) #GPIO.PWM(pin, .f.) create .f. Hz frequency pwm signal
pwm.start(12) #duty cycle (%5)

class App:
    def __init__(self,master):
        frame = Frame(master)
        frame.pack()
        scale = Scale(frame, from_ = 0 , to= 180,
                      orient = HORIZONTAL, command = self.update)
        scale.grid(row=0)
        
    def update(self,angle):
        duty = float(angle) / 10.0 + 2.5
        pwm.ChangeDutyCycle(duty)
        
root = Tk()
root.wm_title('Servo Control')
app = App(root)
root.geometry("350x50+0+0")
root.mainloop()
