import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

trigger_pin = 23
echo_pin = 24

print('HC-SR04 Mesafe Sensoru')

GPIO.setup(trigger_pin,GPIO.OUT) #define input output pins
GPIO.setup(echo_pin,GPIO.IN)

while True:
    GPIO.output(trigger_pin,False) #set trigger off at starting program
    print('Olculuyor..')
    time.sleep(1)
    
    GPIO.output(trigger_pin,True)
    time.sleep(0.00001) #trigger only 10 microseconds (40kHz)
    GPIO.output(trigger_pin,False)
    
    while GPIO.input(echo_pin) ==0:
        pulse_start = time.time() #assign start time when echo off
        
    while GPIO.input(echo_pin)==1:
        pulse_end = time.time() #assign end time when echo detected signal
    
    pulse_duration = pulse_end - pulse_start #calculate time between two pulse
    
    velocity_value = 17404 #at 28.5 Celcius in Izmir July-6
    
    #WHERE IS THE VALUE(17150) COME FROM?
    # x=V*t velocity of sound at 21 Celcius = 343 meters/seconds
    # distance = time(pulse_duration) * velocity of sound(in cm  and V/2 because - 
    #- we calcute time between starting signal and detecting signal, -
    #- this means, we measured the distance of wave twice between the sensor and the obstacle
    # 343 m/s -> 34300 cm/s /2 = 17150 unit
    
    distance = pulse_duration * velocity_value  #calcutate distance as cm
    distance = round(distance,2) #round the value as after comma 2 digit
    
    if distance > 2 and distance <400:
        print("Mesafe : ", distance - 0.5, " cm")
    else:
        print("Olcum araliginde degil")
    
    
    
    
    
    
    
    
    
    
    
    
