import RPi.GPIO as GPIO
from time import sleep

GPIO.setwarnings(False) #disable warnings for gpio lib(optional)
GPIO.setmode(GPIO.BCM) #select GPIO mode as BCM

buzzer_pin = 23 #set buzzer pin as 23 (BCM23, meaning GPIO23 BOARD16)
pir_pin = 18 ##set pir_Sensor pin as 18 (BCM18, meaning GPIO18 BOARD12)
GPIO.setup(buzzer_pin,GPIO.OUT) #define buzzer pin as output
GPIO.setup(pir_pin,GPIO.IN) #define pir_Sensor pin as input

def buzzer(): #define buzzer function    
    #run limited loop:
    for i in range(5):
        GPIO.output(buzzer_pin,GPIO.HIGH) #send high-max voltage
        sleep(.1)
        print('alarm caliyor',i)
j=1        
while True:
    j=j+1
    if GPIO.input(pir_pin):
        print('hareket algilandi')
        buzzer()
    elif GPIO.input(pir_pin) == 0:
        print('bisi yok ',j)
        GPIO.output(buzzer_pin,GPIO.LOW)
    sleep(1)
        
GPIO.cleanup() 



