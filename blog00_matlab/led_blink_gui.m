function varargout = led_blink_gui(varargin)
% LED_BLINK_GUI MATLAB code for led_blink_gui.fig
%      LED_BLINK_GUI, by itself, creates a new LED_BLINK_GUI or raises the existing
%      singleton*.
%
%      H = LED_BLINK_GUI returns the handle to a new LED_BLINK_GUI or the handle to
%      the existing singleton*.
%
%      LED_BLINK_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LED_BLINK_GUI.M with the given input arguments.
%
%      LED_BLINK_GUI('Property','Value',...) creates a new LED_BLINK_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before led_blink_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to led_blink_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help led_blink_gui

% Last Modified by GUIDE v2.5 18-Aug-2020 20:22:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @led_blink_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @led_blink_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before led_blink_gui is made visible.
function led_blink_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to led_blink_gui (see VARARGIN)

%%%%%ACILIS FONKSIYONLARIII%%%%%
clc
clear my_pi %olas� workspace'de �al��an ba�lant�y� kesme %%%%%%%%%
handles.LED_durum = 0; % led'i kapama  %%%%%%%%%
set(handles.led_gosterge, 'BackgroundColor',[0 .5 0]); %gosterge kapama %%%%%%

try %burada bir try-catch ifadesi ile saglikli bir raspi ba�lant�s� yap�yoruz
    my_pi = raspi; %eger raspiye baglanirsan dewamke
catch %sorun varsa uyari penceresinde bunu goruntule
    warndlg('Raspiniz ile ba�lant�da sorun var. L�tfen ba�lan�t�y� kontrol ediniz.')
end

handles.my_pi = my_pi; %raspberry'yi fonksiyonlarda kullanmak i�in handles at�yoruz. %%%%%%%


% Choose default command line output for led_blink_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes led_blink_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = led_blink_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output; %hi�bir �ey ellemiyoruz %%%%%


% --- Executes on button press in on_button.
function on_button_Callback(hObject, eventdata, handles)
% hObject    handle to on_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.led_durum = 1; %led durumu atama %%%%%%%%%
writeLED(handles.my_pi,'led0',1) %led yakma %%%%%%
set(handles.led_gosterge,'BackgroundColor',[0 1 0]); %gosterge ayd�nlatma %%%%%%
%acma butonu bitti %%%%%%%%%%%%%%%%%%%%%


% --- Executes on button press in off_button.
function off_button_Callback(hObject, eventdata, handles)
% hObject    handle to off_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.led_durum = 0; %led durumu atama %%%%%
writeLED(handles.my_pi,'led0',0) %led kapama  %%%%%
set(handles.led_gosterge,'BackgroundColor',[0 .5 0]); %gosterge karartma %%%%%
%kapama butonu bitti %%%%%%%%%%%%%%%%%%%%%

% --- Executes on button press in blink_button.
function blink_button_Callback(hObject, eventdata, handles)
% hObject    handle to blink_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%get fonksiyonu ile her iki degeri alip double'a donusturuyorum. %%%%%%%vvv
kac_defa = str2double(get(handles.kac_defa_text,'String'));
frekans = str2double(get(handles.frekans_text,'String'));

for i=1:kac_defa
    set(handles.led_gosterge,'BackgroundColor',[0 .5 0]);
    writeLED(handles.my_pi,'led0',0);
    pause(frekans);
    set(handles.led_gosterge,'BackgroundColor',[0 1 0]);
    writeLED(handles.my_pi,'led0',1);
    pause(frekans);
end
%acik kalmasin valla yanar falan dolar 7.4 TL
writeLED(handles.my_pi,'led0',0);
set(handles.led_gosterge,'BackgroundColor',[0 .5 0]);

%blink butonu bitti.%%%%%%%%%%%%%%%%%%%%%^^^^^^
%assagida herhangi bir duzenlemem olmadi%%%%%%%%%%%%%%%%%%%%%%^^^^^^^^

% --- Executes on slider movement.
function frekans_text_Callback(hObject, eventdata, handles)
% hObject    handle to frekans_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function frekans_text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to frekans_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_2_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function kac_defa_text_Callback(hObject, eventdata, handles)
% hObject    handle to kac_defa_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of kac_defa_text as text
%        str2double(get(hObject,'String')) returns contents of kac_defa_text as a double


% --- Executes during object creation, after setting all properties.
function kac_defa_text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to kac_defa_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
