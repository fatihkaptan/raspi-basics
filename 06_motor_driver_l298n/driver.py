import RPi.GPIO as GPIO
from time import sleep

#define pins as BCM order
in1 = 24
in2 = 22
en_a = 21
temp1 = 1


GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

#set pins as a output
GPIO.setup(en_a,GPIO.OUT)
GPIO.setup(in1,GPIO.OUT)
GPIO.setup(in2,GPIO.OUT)

#set low voltage when start program
GPIO.setup(in1,GPIO.LOW)
GPIO.setup(in2,GPIO.LOW)

#define pwm control
pwm = GPIO.PWM(en_a,50) #1000 Hertz
if __name__ == '__main__':
    print('burada')
    try:        
        pwm.start(50) # start with 25% duty cycle

        #set direction with high-low voltage
        GPIO.output(in1,GPIO.LOW)
        GPIO.output(in2,GPIO.HIGH)
        sleep(5)
    except KeyboardInterrupt:
        
        print('finitto')
        pass
    print('finitto')
    GPIO.cleanup()


