import RPi.GPIO as io
import time

pin=21
in1 = 24
in2 = 22
io.setmode(io.BCM)
io.setup(pin,io.OUT)
io.setup(in1,io.OUT)
io.setup(in2,io.OUT)

pwm = io.PWM(pin,50)
pwm.start(0)

try:
    while True:
        io.output(in1,io.HIGH)
        io.output(in2,io.LOW)
        time.sleep(1)
        print('burada')
        
        for i in range(100):
            pwm.ChangeDutyCycle(i)
            time.sleep(0.2)
            print(i)

        for i in range(100):
            pwm.ChangeDutyCycle(100-i)
            time.sleep(0.2)
            print(100-i)

except KeyboardInterrupt:
    
    pass

pwm.stop()

io.cleanup()

# while True:
#     try:        
#         print('basladi')
#         io.output(pin,True)
#         time.sleep(3)
#         io.output(pin,False)
#         print('bitti')
#         time.sleep(3)
#         
#     except KeyboardInterrupt:
#         
#         print('kapandı')
#         io.cleanup()
#         pass
# print('kapandı')
# io.cleanup()
